class Campaign
  include ActiveModel::Model

  attr_accessor :x, :y, :z

  ALPHABET = ("A".."Z").to_a

  def generate

    (1..z.to_i).map do |i|
      {
          campaign_name: campaign_name(i),
          price: price,
          target_list: target_list
      }
    end
  end

  def campaign_name(id)
    "campaign#{id}"
  end

  def price
    (rand(0.1) * 100).round(2)
  end

  def target_list
    (0..rand(0..y.to_i-1)).map do |i|
      {
          target: "attr_#{ALPHABET[i]}",
          attr_list: attr_list(ALPHABET[i])
      }
    end
  end

  def attr_list(vowel)
    (0..rand(0..x.to_i-1)).map do |i|
      "#{vowel}#{i}"
    end
  end

end
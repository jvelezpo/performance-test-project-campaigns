class CampaignsController < ApplicationController

  def index
    x = params['x'].to_i
    y = params['y'].to_i
    z = params['z'].to_i

    # Some validation on the incoming data
    # if the values are gt the max value then it is set to the max value
    x = 100 if x > 100
    y = 26 if y > 26
    z = 10000 if z > 10000

    @campaign = Campaign.new(x: x, y: y, z: z)
    render json: @campaign.generate
  end

  ####################################################
  # This two methods are just to show a nice interface
  ####################################################

  def new
    @campaign = Campaign.new
  end

  def create
    @campaign = Campaign.new(params[:campaign])
    @result = @campaign.generate
  end

end
